<div style="zoom:140%">

> The simplicity quickly breaks down when we have multiple components that share a common state:
>
> - Multiple views may depend on the same piece of state.
> - Actions from different views may need to mutate the same piece of state.
>
> For problem one, passing props can be tedious for deeply nested components, and simply doesn't work for sibling components. For problem two, we often find ourselves resorting to solutions such as reaching for direct parent/child instance references or **trying to mutate and synchronize multiple copies of the state via events**.

— https://vuex.vuejs.org/

</div>